// plugins/highlight.js

import Vue from 'vue'
import hljs from 'highlight.js'

// Load a specific language (optional)
import 'highlight.js/styles/monokai-sublime.css' // Choose a style that you prefer

// Define a custom Vue directive for highlighting
Vue.directive('highlight', {
	inserted(el) {
		const nodes = el.querySelectorAll('pre code')
		for (let i = 0; i < nodes.length; i++) {
			hljs.highlightBlock(nodes[i])
		}
	}
})
